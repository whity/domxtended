(function() {
    "use strict";
    
    var DomX = new JS.Class({
        'initialize': function() {
            this._registered = new Object();
        },

        'register': function(cls) {
            name = cls.displayName;

            if (this._registered.hasOwnProperty(name)) {
                throw("class '" + name + "'  already registered");
            }
            
            //support names with multiple levels, example: AA.DD
            var namespace = name.split('.');
            var object = this;
            for (var idx = 0; idx < namespace.length; idx++) {
                if (idx == namespace.length - 1) {
                    object[namespace[idx]] = cls; 
                    break;
                }

                //get object
                if (!object.hasOwnProperty(namespace[idx])) {
                    object[namespace[idx]] = new Object();
                }
                object = object[namespace[idx]];
            }

            //added it to _registered
            this._registered[name] = cls;
        },

        'get': function(name) {
            if (!this._registered.hasOwnProperty(name)) {
                throw("invalid class '" + name + "'");
            }

            return this._registered[name];
        },

        'parse_kwargs': function(node, kwargs) {
            kwargs = kwargs instanceof Object ? kwargs : new Object();

            //check for kwargs comment and merge it
            var kwargs_node = node.previousSibling;
            while (kwargs_node && kwargs_node.nodeType == 3) {
                kwargs_node = kwargs_node.previousSibling;
            }
            if (kwargs_node && kwargs_node.nodeType == 8) {
                var value = kwargs_node.nodeValue.trim();
                if (value.match(/^domx:\s*/i)) {
                    value = JSON.parse(value.replace(/^domx:\s*/i, ''));
                    kwargs = this.hash_merge(value, kwargs);
                    kwargs_node.parentNode.removeChild(kwargs_node);
                }
            }
            
            return kwargs; 
        },

        'hash_merge': function(main, other) {
            for (var key in other) {
                var other_value = other[key];

                //check if key exist in this
                if (main.hasOwnProperty(key)) {
                    var value = main[key];
                    if (value instanceof Object && other_value instanceof Object) {
                        other_value = DomX.hash_merge(main, other_value);
                    }
                }

                main[key] = other_value;
            } 

            return main;
        }
    });
    DomX = new DomX(); 

    //config object
    DomX.register(new JS.Class('Config', {
        'initialize': function(kwargs) {
            kwargs = kwargs instanceof Object ? kwargs : new Object();
            this._data = kwargs;
        },

        'get': function(key) {
            var value = null;
            if (key == null || key == undefined) {
                value = JSON.parse(JSON.stringify(this._data));
            }
            else if (this._data[key]) {
                value = this._data[key];
            }

            return value;
        }
    }));
    
    DomX.register(new JS.Class('Element', {
        'initialize': function(node, kwargs) {
            this._kwargs = DomX.parse_kwargs(node, kwargs);
            this._node = node;

            this.name = this._kwargs['name'];
            this.context_name = this._kwargs['context_name'];
            this.templates = new Object();

            this.parent = undefined;
            this._children = new Array();

            //search for children
            this._search_children();
        },

        '_search_children': function(node) {
            if (!node) {
                node = this._node;
            }

            var children = node.children;
            var total_children = children.length;

            for (var idx = 0; idx < total_children; idx++) {
                var item = children[idx];

                if (item.hasAttribute('data-domx-class')) {
                    var cls = DomX.get(item.getAttribute('data-domx-class'));
                    var obj = new cls(item);

                    //check if is a template
                    if (obj.isA(DomX.Template)) {
                        //remove from dom
                        item.parentNode.removeChild(item);
                        idx -= 1;
                        total_children -= 1;
                        
                        //if name isn't defined, continue to the next item
                        if (!obj.name) {
                            continue;
                        }
                        
                        //add template to object
                        this.templates[obj.name] = obj;
                    }
                    else {
                        //append object
                        this._add_child(obj, function(child) { 
                            this._children.push(child); 
                        });
                    }
                }
                else {
                    this._search_children(item);
                }

                //destroy $item
                item = null;
            }

            //destroy $node var
            node = null;
        },

        '_get_child_index': function(child) {
            if (typeof(child) == 'string') {
                if (child.charAt(0) != '$') {
                    child = '$' + child;
                }
                
                child = this[child];
            }

            if (child instanceof DomX.Element) {
                for (var idx = 0; idx < this._children.length; idx++) {
                    var item = this._children[idx];
                    if (item.equals(child)) {
                        child = idx;
                        break; //stop loop, child found
                    }
                }
            }
            
            if (child == undefined || child == null || typeof(child) != 'number') {
                child = -1;
            } 

            return child;
        },

        'getChild': function(child) {
            child = this._get_child_index(child);
            if (child != null && child != undefined && this._children.length > 0 && child >= 0 && child < this._children.length) {
                return this._children[child];
            }

            return;
        },

        '_add_child': function(obj, func) {
            //if object already exist don't add it
            if (this.getChild(obj)) {
                return false;
            }

            //remove from dom and previous parent
            obj.remove();

            //set parent
            obj.parent = this;

            //alias
            var alias = [
                {'attr': 'context_name', 'on': this},
                {'attr': 'name', 'on': DomX.App}
            ];
            for (var idx in alias) {
                var item = alias[idx];

                //create object attribute to child
                if (obj.hasOwnProperty(item['attr']) && obj[item['attr']]) {
                    var on_name = '$' + obj[item['attr']];
                    if (item['on'].hasOwnProperty(on_name)) {
                        throw("duplicated '" + on_name + "'");
                    }

                    item['on'][on_name] = obj;
                }
            }

            //add to children array
            func.call(this, obj);

            return true;
        },
        
        'equals': function(other) {
            if (other && this._node == other._node) {
                return true;
            }

            return false;
        },
        
        //remove child
        'removeChild': function(child) {
            //get child
            var child_idx = this._get_child_index(child);
            if (child_idx == -1) {
                return;
            }    

            child = this._children[child_idx];
            
            //reset parent
            child.parent = null;

            //remove attribute
            var child_name = child.name;
            if (child_name && this.hasOwnProperty('$' + child_name)) {
                delete(this['$' + child_name]);
            }

            //remove from children
            this._children.splice(child_idx, 1);
             
            //remove from dom
            child._node.parentNode.removeChild(child._node);

            return child;
        },

        //remove object from parent and dom
        'remove': function() {
            //if parent, remove it
            var parent = this.parent;
            if (parent) {
                parent.removeChild(this);
            }
            
            return this;
        },
        
        //remove all chidren
        'removeChildren': function() {
            var index = this._children.length - 1;
            while (index >= 0) {
                this.removeChild(index);

                index -= 1;
            }

            return this;
        },

        //appendChild
        'appendChild': function(obj) {
            //append to this object
            this._add_child(obj, function(child) { 
                this._children.push(child); 
                this._node.appendChild(child._node);
            });

            return this;
        },

        //prepend child
        'prependChild': function(obj) {
            this._add_child(obj, function(child) {
                //get first child
                var first_child = this.getChild(0);
                
                //prepend to children array
                this._children.unshift(child);

                //prepend it to dom
                this._node.insertBefore(child._node, first_child ? first_child._node : null);
            });

            return this;
        },

        'hasClass': function(value) {
            return this._node.classList.contains(value);
        },

        'addClass': function(value) {
            this._node.classList.add(value);
            return this;
        },

        'removeClass': function(value) {
            this._node.classList.remove(value);
            return this;
        },

        'show': function() {
            this._node.classList.remove('hidden');
            return this;
        },

        'hide': function() {
            this._node.classList.add('hidden');
            return this;
        },

        '_next_previous_child': function(child, inc) {
            //get child index
            var index = this._get_child_index(child);
            if (index == -1) {
                return;
            }
            
            index += inc;
            if (index < 0 || index >= this._children.length) {
                return;
            }

            return this._children[index];
        },

        //return next child 
        'nextChild': function(child) {
            return this._next_previous_child(child, 1);
        },

        //return previous child
        'previousChild': function(child) {
            return this._next_previous_child(child, -1);
        },

        '_next_previous': function(name) {
            var parent = this.parent;
            if (parent) {
                return parent[name + 'Child'](this);
            }

            return;
        },

        //return next sibling
        'next': function() {
            return this._next_previous('next');
        },

        //return previous sibling
        'previous': function() {
            return this._next_previous('previous');
        },
        
        //bind a function as an event handler        
        '_bind': function (name) {
            var fn = this[name];
            var that = this;
            //this._$.bind(name, function(event) {
            //    return fn.call(that, event);
            //});
            
            this._node.addEventListener(name, function(event) {
                return fn.call(that, event);
            });

            return this;
        },
        
        //clone object
        'clone': function() {
            var clone = this._node.cloneNode(true);
            clone = new this.klass(clone);
            clone.name = undefined;   

            return clone;
        },

        //update object
        'update': function(data) {
            throw("method not implemented");
        }
    }));
    
    //application object
    DomX.register(new JS.Class("Application", DomX.Element, {
        'initialize': function(kwargs) {
            this.config = new DomX.Config(kwargs);

            //set DomX.App on window
            window.DomX.App = this;
            
            this._onstart_queue = new Array();

            //init root object
            this.callSuper(window.document.body);
        },
        
        'start': function() {
            //execute registered callbacks on queue
            this._on_start();

            return this;
        },

        'onStart': function(callback, priority) {
            priority = parseInt(priority);
            if (priority == 'NaN') {
                priority = 1000;
            }

            this._onstart_queue.push({'callback': callback, 'priority': priority});
        },

        '_on_start': function() {
            //sort queue
            this._onstart_queue.sort(function(a, b) { return a['priority'] - b['priority']; });

            //execute elements on queue
            for (var idx = 0; idx < this._onstart_queue.length; idx++) {
                var item = this._onstart_queue[idx];
                if (typeof(item['callback']) == 'function') {
                    item['callback']();
                }
            }

            //delete onstart_queue
            delete(this['_onstart_queue']);
        }
    }));
    
    /***TEMPLATES***/
    DomX.register(new JS.Class('Template', {
        'initialize': function(node, kwargs) {
            this._kwargs = DomX.parse_kwargs(node, kwargs);

            this.name = this._kwargs['name'];
            this._template = this._template(node.innerHTML.trim());
        },

        '_template': function(str) {
            throw("method not implemented");
        },

        'render': function(data) {
            var container = window.document.createElement("div");
            container.innerHTML = this._template(data);

            //get first child that not a comment or text
            container = container.firstChild;
            while (container && (container.nodeType == 3 || container.nodeType == 8)) {
                container = container.nextSibling;
            }
            if (!container) {
                return;
            } 

            var cls = DomX.get(container.getAttribute('data-domx-class'));

            return new cls(container);
        }
    }));

    DomX.register(new JS.Class('Template.doT', DomX.Template, {
        '_template': function(str) {
            return doT.template(str);
        }
    }));

    DomX.register(new JS.Class('Template.Jiko', DomX.Template, {
        '_template': function(str) {
            return jiko.loadTemplate(str);
        }
    }));
    /***************************/

    window.DomX = DomX;
})(window);
